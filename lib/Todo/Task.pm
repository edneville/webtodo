# WebTodo, a cgi-based todolist system
# Copyright (C) 1999-2000 British Broadcasting Corporation
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# See the COPYING file for details.

package Todo::Task;

use Date::Format;
use Todo::Config;
use Todo::General;
use Todo::List;
use Todo::Access;

sub new {
	my($class, $id) = @_;
	my $self = {};
	bless $self,ref $class || $class || "Todo::Task";
	$self->{'id'} = $id;
	$self->{'editing_user'} = $ENV{"REMOTE_USER"};
	return $self;
}

sub read_task {
	my $self = shift;
	my $file=shift;
	my $accessuser=shift;
	my ($dir, $type, $time, $user, $data, $submittime);
	$file =~ /(.*)\/(task\.[0-9-]+)$/;
	$dir = $1; $file = $2;
	#return 0 unless ($dir eq $self->{'dir'});
	return 0 unless ($file =~ /task\.([0-9-]+)$/);
	$self->{'id'} = $1;
	$self->{'bbe'} = 0;
	$self->{'mtime'} = 0;
	$self->{'priority'} = 2;
	$self->{'list'} = "engineers";
	$self->{'state'}=1;
	$self->{'private'}=0;
	if ( -f "$dir/$file" ) {
		return 0 unless open(F, "$dir/$file");
	} else {
		return 0 unless open(F, "$dir/old/$file");
	}
	while(<F>) {
		chop;
		($type, $time, $_) = /^(~?.)([0-9]+):(.*)/;
		($user, $data) = split(/:/,$_,2);
		$data = Todo::General::unescape($data);
		$data =~ s/\&/\&amp;/g;
		$data =~ s/\"/\&quot;/g;
		$data =~ s/\</\&lt;/g;
		$data =~ s/\>/\&gt;/g;
		$data =~ s/\r//mg;
		$data =~ s/\s*$//s;
		$data =~ s/\n/<BR>/mg;
		$secret = ($type =~ s/^~//) || 0;
		unless (($time eq $submittime) && ($type ne 'e')) {
			if((($secret)&&(Todo::Access::check_list(join(",",Todo::Access::list_groups($user)),$accessuser)))||(!$secret))
				{
				push @{$self->{'type'}},$type;
				push @{$self->{'secret'}},$secret;
				push @{$self->{'time'}},$time;
				push @{$self->{'user'}},$user;
				push @{$self->{'data'}},$data;
				}
		}
		$submittime = $time unless ($submittime);
		if ($type eq '+') {
			if (!$self->{'subject'}) {
				$_ = $data;
				($self->{'subject'}) = /Subject:\s*([^<]*)(<BR>|$)/;
				if (!$self->{'subject'}) {
					($self->{'subject'}) = /^([^<]*)(<BR>|$)/;
				}
			}
		}
		if ($type eq 'h') { $self->{'private'}=$data; }
		if ($type eq 'r') { $self->{'priority'}=$data; }
		if ($type eq 'c') { $self->{'custemail'}=$data; }
		if ($type eq 'C') { $self->{'custnotif'}=$data; }
		if ($type eq 'p') { $self->{'state'}=2; }
		if ($type eq '-') { $self->{'state'}=3; }
		if ($type eq '=') { $self->{'state'}=1; }
		if ($type eq '~') { $self->{'state'}=1; }
		if ($type eq 's') { $self->{'subject'}=$data; }
		if ($type eq 'b') { $self->{'bbe'}=$data; }
		if ($type eq 'l') {
			$self->{'list'}=$data;
			if($self->{'accesslist'} ne "") {
				$self->{'accesslist'}.=",";
			}
			$self->{'accesslist'} .= join(",",Todo::Access::list_groups($data)).",$data";
			$self->{'state'}=3 if ($data eq "junk");
		}
		$self->{'luser'} = $user;
		$self->{'mtime'} = $time;
		if($self->{'accesslist'} ne "")
			{
			$self->{'accesslist'}.=",";
			}
		$self->{'accesslist'} .= join(",",Todo::Access::list_groups($user)).",$user";
		my %accesslist;
		foreach (split(",",$self->{'accesslist'}))
			{
			next if($_ eq "");
			$accesslist{$_}=1;
			}
		$self->{'accesslist'} = join(",",keys %accesslist);
	}
	close(F);
	if($self->{"list"}eq "")
		{
		$self->{"list"}="engineers";
		}
	if(($self->{"private"})&&
		(!$self->check_access($accessuser)))
		{
		return 0;
		}
	return 1;
	#return $self->{'id'};
}

sub iterate_events {
	my $self = shift;
	my $direction = shift;
	if ($direction == 1) {
		$self->{'index'} = -1;
		$self->{'last'} = 1 + $#{$self->{'type'}};
		$self->{'direction'} = 1;
	} else {
		$self->{'index'} = 1 + $#{$self->{'type'}};
		$self->{'last'} = -1;
		$self->{'direction'} = -1;
	}
}

sub next_event {
	my $self = shift;
	$self->{'index'} += $self->{'direction'};
	my $i = $self->{'index'};
	return 0 if ($i == $self->{'last'});
	return (${$self->{'type'}}[$i], ${$self->{'time'}}[$i], 
	        ${$self->{'user'}}[$i], ${$self->{'data'}}[$i],
		${$self->{'secret'}}[$i]);
}

# Correct sequence for an edit/create:
# $task->set_user("username");
# $task->event_something($secret, $data);
# (stand-in: event_generic($secret, $data, $type);
# ...
# $task->flush_events();

sub check_access
	{
    my $self = shift;
    my $user = shift;
    if($self->{'accesslist'} eq "")
    	{
    	return 1;
    	}
    return Todo::Access::check_list($self->{'accesslist'},$user);
	}

sub lock {
    my $self = shift;
	return 0 if $self->{'locked'};
	return 1 if ($self->{'id'} eq "");
	print "<P>lock: id is fine</P>\n" if ($self->{'debug'});
	return 1 if (!Todo::General::creat("$Todo::Config::tasks_dir/lock.$self->{'id'}"));
	print "<P>lock: succeeded</P>\n" if ($self->{'debug'});
	$self->{'locked'} = 1;
	return 0;
}

sub unlock {
	my $self = shift;
	unlink ("$Todo::Config::tasks_dir/lock.$self->{'id'}");
	$self->{'locked'} = undef;
	print "<P>unlock: unlinked $Todo::Config::tasks_dir/lock.$self->{'id'}</P>\n" if ($self->{'debug'});
}

sub set_user {
	my $self = shift;
	my $user=shift;
	$self->{'editing_user'} = $user;
	return if(!$self->check_access($self->{'editing_user'}));
	my %accesslist;
	foreach (split(",",$self->{'accesslist'}))
		{
		$accesslist{$_}=1;
		}
	foreach (Todo::Access::list_groups($self->{'editing_user'}))
		{
		$accesslist{$_}=1;
		}
	$accesslist{$self->{'editing_user'}}=1;
	$self->{'accesslist'} = join(",",keys %accesslist);
}

sub set_secret {
	my $self = shift;
	return if(!$self->check_access($self->{'editing_user'}));
	$self->{'secret'} = shift;
}

sub add_event {
	my ($self, $data, $type) = @_;
	my $t = $self->{'fixed_time'} || time;
	$self->{'event_cache'} .= "~" if ($self->{'secret'} && $type !~ /[blrceC+]/);
	$self->{'event_cache'} .= "$type$t:$self->{'editing_user'}:$data\n";
}

sub set_private {
	my $self = shift;
	my $private=shift;
	return if(!$self->check_access($self->{'editing_user'}));
	if ($self->{'private'} ne $private) {
		$self->{'private'} = $private;
		$self->add_event($private, 'h');
	}
}

sub set_subject {
	my ($self, $subject) = @_;
	return if(!$self->check_access($self->{'editing_user'}));
	if ($self->{'subject'} ne $subject) {
		$self->{'subject'} = $subject;
		$self->add_event(Todo::General::escape($subject), 's');
	}
}

sub set_duedate {
	my ($self, $duedate) = @_;
	return if(!$self->check_access($self->{'editing_user'}));
	if ($self->{'bbe'} != $duedate) {
		if (($duedate > 5) && ($duedate > $self->{'bbe'})) {
			$self->{'bbe'} = $duedate;
			$self->add_event($duedate, 'b');
		}
	}
}

sub set_assigned {
	my ($self, $assigned) = @_;
	return if(!$self->check_access($self->{'editing_user'}));
	if ($self->{'list'} ne $assigned) {
		$self->{'list'} = $assigned;
		$self->add_event($assigned, 'l');
		my %accesslist;
		foreach (split(",",$self->{'accesslist'}))
			{
			$accesslist{$_}=1;
			}
		foreach (Todo::Access::list_groups($assigned))
			{
			$accesslist{$_}=1;
			}
		$accesslist{$assigned}=1;
		$self->{'accesslist'} = join(",",keys %accesslist);
	}
}

sub set_priority {
	my ($self, $priority) = @_;
	return if(!$self->check_access($self->{'editing_user'}));
	if ($self->{'priority'} != $priority) {
		$self->{'priority'} = $priority;
		$self->add_event($priority, 'r');
	}
}

sub set_pending {
	my ($self, $data) = @_;
	return if(!$self->check_access($self->{'editing_user'}));
	$self->{'state'} = 2;
	$self->add_event(Todo::General::escape($data), 'p');
	$self->{'lastdata'} = $data;
}

sub set_done {
	my ($self, $data) = @_;
	return if(!$self->check_access($self->{'editing_user'}));
	$self->{'state'} = 3;
	$self->add_event(Todo::General::escape($data), '-');
	$self->{'lastdata'} = $data;
}

sub set_text {
	my ($self, $data) = @_;
	return if(!$self->check_access($self->{'editing_user'}));
	$self->{'state'} = 1;
	$self->add_event(Todo::General::escape($data), '=');
	$self->{'lastdata'} = $data;
}

sub set_email {
	my ($self, $email) = @_;
	return if(!$self->check_access($self->{'editing_user'}));
	$email = Todo::General::parse_emaillist($email);
	if ($self->{'custemail'} ne $email) {
		$self->{'custemail'} = $email;
		$self->add_event($email, 'c');
	}
}

sub set_willmail {
	my ($self, $willmail) = @_;
	return if(!$self->check_access($self->{'editing_user'}));
	if($self->{'custemail'} ne "") {
		$self->{'willmail'} = $willmail;
		$self->{'event_cache'} =~ s/^e[^\n]*\n//mg;
		if ($willmail) {
			$self->add_event($self->{'custemail'}, 'e');
		}
	}
}

sub set_notify {
	my ($self, $notif) = @_;
	return if(!$self->check_access($self->{'editing_user'}));
	if ($self->{'custnotif'} != $notif) {
		$self->{'custnotif'} = $notif;
		$self->add_event($notif, 'C');
	}
}

sub flush_events {
	my $self = shift;
	my $debug = $self->{'debug'};
	my $id = $self->{'id'};
	my $data;
	print "<P>flush_events called</P>\n" if ($debug);
	if ($self->lock()) {
		print "<P>FAILED: lock $self->{'id'}</P>\n" if ($debug);
		return 1;
	}
	if (Todo::List::open_todolist($Todo::Config::tasks_dir, $Todo::Config::dbmfile, 1)) {
		$self->unlock();
		return 1;
	}
	if (!open(F, ">>$Todo::Config::tasks_dir/task.$id")) {
		print "<P>FAILED: open $Todo::Config::tasks_dir/task.$id</P>\n" if ($debug);
		$self->unlock();
		return 1;
	}
	print "<P>SUCCESS: open $Todo::Config::tasks_dir/task.$id</P>\n" if ($debug);
	$Todo::List::dbm{$id} = "$self->{'list'}:".Todo::General::escape($self->{'subject'}).":".Todo::General::escape($self->{'custemail'}).":".time.":$self->{'state'}:$self->{'bbe'}:$self->{'priority'}:".Todo::General::escape($self->{'accesslist'});
	Todo::List::close_todolist();
	print F $self->{'event_cache'};
	close(F);
	$self->unlock();
	$self->{'event_cache'} =~ s/^~?[p=+-][0-9]+:[^:]+:([^\n]*)\n/$data=$1/emgo;
	$self->{'event_cache'} = "";
	$self->{'fixed_time'} = undef;
	$data=Todo::General::unescape($data);
	if ($self->{'willmail'}) {
		$self->mailout($data);
	}
	return 0;
}

#
# call this with the initial data to be entered into the task
# the rest of the events should be added by normal set_* calls and then
# flushed
#

sub newtask {
	my ($self, $data) = @_;
	my $hr, $min, $sec, $tmpid;
	$count = 5;
	($sec,$min,$hr) = (localtime(time))[0..2];
	$tmpid = $hr*36000 + $min*600 + $sec*10;
	for (;; $tmpid++) {
		if (!--$count) {
			return 0;
		}
		$id = time2str("%Y%m%d-",time).sprintf("%06d",$tmpid);
		$self->{'id'} = $id;
		next if ($self->lock());
		if (!Todo::General::creat("$Todo::Config::tasks_dir/task.".$id)) {
			$self->unlock();
			next;
		}
		$self->{'state'} = 1;
		$self->{'fixed_time'} = time;
		$self->{'newtask'} = 1;  # used by mailout
		$self->add_event(Todo::General::escape($data), '+');
		return $id;
	}
}

# TODO: make this use a template.  requires more code in the template
# script (to detect @{%-20s,varname} for example)

sub mailout {
	my ($self, $data) = @_;
	my %status_names = ("0" => "NEW TASK", "1" => "IN PROGRESS", "2" => "PENDING YOUR RESPONSE", "3" => "DONE");
	my %pri_names = ("1", "Urgent", "2", "Normal", "3", "Background");
	my $status = $self->{'state'};
	my $sendtolist, $sendfromlist;

	$status = 0 if ($self->{'newtask'});

	if (open(GROUP, $Todo::Config::alias)) {
		while(<GROUP>) {
			chop;
			($l,$r)=split(/:/);
			$listmail{$l} = $r;
		}
		close(GROUP);
	} else {
		print STDERR "Cannot ",$Todo::Config::alias,": $!\n";
	}
	$list=$self->{"list"};
	$sendtolist = $listmail{$list} || $Todo::Config::defaultemail;
	$sendfromlist = $listmail{$list} || $Todo::Config::defaultemail;
	$sendfromlist = $Todo::Config::defaultemail if ($sendfromlist =~ /,/);
#	$sendfromlist ="todolist-".$self->{'id'};
	if ($self->{'custnotif'} > 1 && $self->{'custemail'}=~/[^\s@| ]+@[^\s@| ]+/) {
		if (open (SENDMAIL, "|/usr/lib/sendmail -oi -t")) {
			print SENDMAIL 
"From: $sendfromlist
To: $self->{'custemail'}
cc: $sendtolist
Subject: $self->{'subject'}
";
			printf SENDMAIL "
     Task id: %-20s    Status: %s
Last edit by: %-20s  Priority: %s
	List: %-20s  Due date: %s
", $self->{'id'}, $status_names{$status}, $self->{'editing_user'},
	$pri_names{$self->{'priority'}}, $self->{'list'}, time2str("%Y-%m-%d",$self->{'bbe'});
			print SENDMAIL "
-----BEGIN TASK DATA-----
$data
------END TASK DATA------

To check the progress of this task, go to
$Todo::Config::cgi_prefix/$Todo::Config::list_cgi?id=$self->{'id'}
- user guest, password guest.

Please leave the above URL (or at least the task number) in any
email correspondence - this will help us respond more quickly.

This perl script values your custom and hopes you have a nice day.
";
			close(SENDMAIL);
		}
	}
}

sub DESTROY { }
sub AUTOLOAD {
	my $self = shift;
	my $type = ref($self) || die "$self is not an object";
	my $var = $AUTOLOAD;
	$var =~ s/.*://;
	return $self->{$var};
}

1;
