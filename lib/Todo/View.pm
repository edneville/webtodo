# WebTodo, a cgi-based todolist system
# Copyright (C) 1999-2000 British Broadcasting Corporation
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# See the COPYING file for details.

# stuff dealing with view information

package Todo::View;

use Todo::Access;

sub new {
	my $self={};
	my $class=shift;
	
	bless $self,$class;
	$self->{"name"}=shift || "self";
	$self->{"access"}=shift || "";
	$self->{"owner"}=shift || "";
	$self->{"subject"}=shift || "";
	$self->{"email"}=shift || "";
	return $self;
}

sub sortbyname {
	$a->{"name"} cmp $b->{"name"};
}

sub get_view {
	my $views=shift;
	my $view=shift;
	my $user=shift;
	my $v;
	my $g;
	
	foreach $v (@{$views}) {
		if($view eq $v->{"name"}) {
			return $v;
		}
	}
	return undef;
}

sub read_views {
	my @views;
	local *VIEWS;
	my ($view,$owner,$subject,$email);
	my $username=shift || $ENV{"REMOTE_USER"};
	my %match;

	push @lists,new Todo::View($listuser,$listuser,"^$listuser\$");
	if (open(VIEWS, $Todo::Config::views)) {
		while(<VIEWS>) {
			chop;
			if ( /^#/ ) {
				next;
			}
			($view,$access,$owner,$subject,$email)=split(/:/);
			next if($match{$view});
			if((Todo::Access::check_list($access,$username))||($access eq "")) {
				$view=new Todo::View($view,$access,$owner,$subject,$email);
				$views[$#views+1]=$view;
				$match{$view->{"name"}}=1;
			}
		}
		close(VIEWS);
	}
	return @views;
}

1;
