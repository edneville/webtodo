# WebTodo, a cgi-based todolist system
# Copyright (C) 1999-2000 British Broadcasting Corporation
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# See the COPYING file for details.

package Todo::Prefs;

use Todo::Config;

1;
