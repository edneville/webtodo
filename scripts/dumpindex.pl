#!/usr/local/bin/perl
#
# Query the index
#

# WebTodo, a cgi-based todolist system
# Copyright (C) 1999-2001 British Broadcasting Corporation
# by Ciaran Anscomb <ciarana@rd.bbc.co.uk>,
#    Chris Turner <chris-tl@gathering.org.uk>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# See the COPYING file for details.

use lib "/usr/local/todo/lib";
use Todo::Config;
use Todo::Task;
use Fcntl;
use GDBM_File;

tie %index, "GDBM_File", "$Todo::Config::tasks_dir/$Todo::Config::dbmfile",&GDBM_READER,0666 || die "tie: $!";

foreach $id (keys %index)
	{
	print $id,"=",$index{$id},"\n";
	}
untie %index;
