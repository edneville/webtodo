#!/usr/local/bin/perl
#
# Rebuilds the GDBM index
#

# WebTodo, a cgi-based todolist system
# Copyright (C) 1999-2001 British Broadcasting Corporation
# by Ciaran Anscomb <ciarana@rd.bbc.co.uk>,
#    Chris Turner <chris-tl@gathering.org.uk>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# See the COPYING file for details.

use lib "/usr/local/todo/lib";
use Todo::Config;
use Fcntl;
use GDBM_File;

#$debug = 1;
$limit_donetime = $Todo::Config::done_timeout*24*60*60;

tie(%index, "GDBM_File", "$Todo::Config::tasks_dir/$Todo::Config::dbmfile", &GDBM_WRCREAT, 0660) || die "tie: $!";
tie(%oldindex, "GDBM_File", "$Todo::Config::tasks_dir/old/$Todo::Config::dbmfile", &GDBM_WRCREAT, 0660) || die "tie: $!";

$t = time;

foreach $id (keys %index) {
	($assigned,$subject,$email,$mtime,$state,$due,$priority,$accesslist)=
		split(/:/,$index{$id});
	$age = $t - $mtime;
	print STDERR "$id: time=$t, mtime=$mtime, age=$age, limit=$limit_donetime\n" if ($debug && $state == 3);
	if (($state == 3) && ($age >= $limit_donetime)) {
		print STDERR "Moving $id... " if ($debug);
		if (rename "$Todo::Config::tasks_dir/task.$id","$Todo::Config::tasks_dir/old/task.$id") {
			$oldindex{$id} = $index{$id};
			delete $index{$id};
			print STDERR "done.\n" if ($debug);
		} else {
			print STDERR "FAILED.\n" if ($debug);
		}
	}
}
	
untie %oldindex;
untie %index;
chown((getpwnam($Todo::Config::httpd_user))[2], (getgrnam($Todo::Config::task_group))[2], "$Todo::Config::tasks_dir/old/$Todo::Config::dbmfile", "$Todo::Config::tasks_dir/$Todo::Config::dbmfile");
chmod 0660, "$Todo::Config::tasks_dir/old/$Todo::Config::dbmfile", "$Todo::Config::tasks_dir/$Todo::Config::dbmfile";
